# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Thi Project is microservice for data customer

### Technology ###

* ASP.NET Core 6

### Details ###
this project use sqllocaldb, so if you want running this project in your local computer following this step:
* check your local db version
 `sqllocaldb versions`
* create local db
`sqllocaldb create Customer [Vesion]`
* import database from file use SSMS (Sql Server Management Studio)
    ##
    1. Open SSMS
    2. login `(LocalDb)\Customer`
    3. klik databases -> Import Data-tier application
    4. import from local disk and select file database from repository with name customer.bacpac
    5. next until finish

* open package manage console
`update-database`
* 
