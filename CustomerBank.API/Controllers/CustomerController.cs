﻿using CustomerBank.API.Data;
using Microsoft.AspNetCore.Http;
using CoreApiResponse;
using Microsoft.AspNetCore.Mvc;
using CustomerBank.API.Model;
using CustomerBank.API.Data.Model;

namespace CustomerBank.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly CustomerDbContext dbContext;

        public CustomerController(CustomerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        [Route("{email}")]
        public IActionResult Get([FromRoute] string email)
        {
            var customer = dbContext.customers.Where(x=>x.Email == email).FirstOrDefault();
            if (customer == null)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            Model.CustomerDTOModel response = new()
            {
                ID = customer.ID,
                Name = customer.Name,
                Email = customer.Email,
                DateOfBirth = customer.DateOfBirth,
                Password = customer.Password,
                SavingsAccountID = customer.SavingsAccountID.GetValueOrDefault()
            };
            return CustomResult("Success", response, System.Net.HttpStatusCode.OK);
        }

        [HttpPost]
        public IActionResult Add([FromBody] CustomerDTOModel request)
        {
            try
            {
                var customer = new Customer
                {
                    ID = Guid.NewGuid(),
                    Name = request.Name,
                    Email = request.Email,
                    Password = request.Password,
                    DateOfBirth = request.DateOfBirth,
                    SavingsAccountID = new Guid()
                };
                dbContext.customers.Add(customer);
                dbContext.SaveChanges();
            }
            catch(Exception ex)
            {
                return CustomResult(ex.ToString(), System.Net.HttpStatusCode.ExpectationFailed);
            }
            return  CustomResult("Success", System.Net.HttpStatusCode.OK);
        }

        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginDTO request)
        {
            var customer = dbContext.customers.Where(x=> x.Email.Equals(request.email) && x.Password.Equals(request.password)).FirstOrDefault();
            var listCus = dbContext.customers.ToList();
            if(customer == null) { return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound); }
            
            return CustomResult("Success", customer, System.Net.HttpStatusCode.OK);
        }

        [HttpPost("update")]
        public IActionResult Update([FromBody] CustomerDTOModel request)
        {
            var customer = dbContext.customers.Find(request.ID);
            if(customer == null)
            {
                return CustomResult("failed update", System.Net.HttpStatusCode.NotFound);
            }
            customer.Name = request.Name;
            customer.Email = request.Email;
            customer.Password = request.Password;
            customer.DateOfBirth = request.DateOfBirth;
            dbContext.SaveChanges();

            return CustomResult("success", customer, System.Net.HttpStatusCode.OK);
        }

        [HttpPost("add-saving-account")]
        public IActionResult AddSavingAccount([FromBody] UpdateCustomerSavingsAccountIdModel request)
        {
            var customer = dbContext.customers.Find(request.CustomerID);
            if (customer == null)
            {
                return CustomResult("Not Found", System.Net.HttpStatusCode.NotFound);
            }

            customer.SavingsAccountID = request.SavingsAccountID;
            dbContext.SaveChanges();

            return CustomResult("success", System.Net.HttpStatusCode.OK);
        }
    }
}
