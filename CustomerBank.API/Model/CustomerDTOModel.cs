﻿namespace CustomerBank.API.Model
{
    public class CustomerDTOModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Guid SavingsAccountID { get; set; }
    }
}
