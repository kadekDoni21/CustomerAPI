﻿namespace CustomerBank.API.Model
{
    public class LoginDTO
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
