﻿using CustomerBank.API.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace CustomerBank.API.Data
{
    public class CustomerDbContext : DbContext
    {
        public CustomerDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {
            
        }

        public DbSet<Customer> customers { get; set; }
    }
}
